package pl.edu.pjwstk;

import org.junit.Test;
import pl.edu.pjwstk.db.OrderManager;
import pl.edu.pjwstk.service.Address;
import pl.edu.pjwstk.service.ClientDetails;
import pl.edu.pjwstk.service.Order;
import pl.edu.pjwstk.service.OrderItem;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vpnk on 31.10.16.
 */
public class OrderManagerTest {
    OrderManager orderManager = new OrderManager();

    private static final ClientDetails CLIENT_1 = new ClientDetails("Pawel", "Wilk", "WilkP");
    private static final Address ADDRESS_1 = new Address("Śniadeckich", "35", "61", "80-808", "Gdańsk", "Polska");
    private static final ArrayList<OrderItem> ORDER_ITEM_LIST_1 = new ArrayList<OrderItem>();
    @Test
    public void checkConnection(){
        assertNotNull(orderManager.getConnection());
    }

    @Test
    public void checkAdding(){

        Order order = new Order(CLIENT_1, ADDRESS_1, ORDER_ITEM_LIST_1);
        orderManager.clearOrders();
        assertEquals(1, orderManager.addOrder(order));

        List<Order> orderList = orderManager.getAllOrders();
        Order receivedOrder = orderList.get(0);

        assertEquals(CLIENT_1, receivedOrder.getClient());
        assertEquals(ADDRESS_1, receivedOrder.getDeliveryAddress());
        assertEquals(ORDER_ITEM_LIST_1, receivedOrder.getItems());


    }

}