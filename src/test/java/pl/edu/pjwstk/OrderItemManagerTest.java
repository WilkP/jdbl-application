package pl.edu.pjwstk;

import org.junit.Test;
import pl.edu.pjwstk.db.OrderItemManager;
import pl.edu.pjwstk.service.OrderItem;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vpnk on 31.10.16.
 */
public class OrderItemManagerTest {
    private static final String NAME_1 = "Herbata";
    private static final String DESCRIPTION_1 = "Świeże pączki zielonej herbaty gyokuro zbieranej przez" +
            " medytujących przez 20 lat mnichów za pomocą złotych nożyczek";
    private static final double PRICE_1 = 19.99;

    OrderItemManager orderItemManager = new OrderItemManager();

    @Test
    public void checkConnection(){
        assertNotNull(orderItemManager.getConnection());
    }

    @Test
    public void checkAdding(){
        OrderItem orderItem = new OrderItem(NAME_1, DESCRIPTION_1, PRICE_1);

        orderItemManager.clearOrderItems();
        assertEquals(1, orderItemManager.addOrderItem(orderItem));

        List<OrderItem> orderItemList = orderItemManager.getAllOrderItems();
        OrderItem receivedOrderItem = orderItemList.get(0);

        assertEquals(NAME_1,receivedOrderItem.getName());
        assertEquals(DESCRIPTION_1,receivedOrderItem.getDescription());
        assertEquals(PRICE_1,receivedOrderItem.getPrice(), 0.01);
    }
}